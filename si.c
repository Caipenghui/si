/*
 * Si.c   this is si 
 *
 * Copyright (C) 2014-2018  Caipenghui  蔡鹏辉  <Caipenghui_c@163.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

typedef struct {
        int  ID;
        int  math;
        int  English;
        int  C;
        int  avargrade;
        char name[20];
}Stu;

int main(void)
{
        FILE *fp;
        Stu stu[5];
   
        int i, avargrade=0;
        printf("请输入5个同学的信息：学生号，姓名，3门成绩:\n");
        for (i=0; i<5; i++) {
                scanf("%d %s %d %d %d", &(stu[i].ID),stu[i].name, &(stu[i].math), &(stu[i].English), &(stu[i].C));
                stu[i].avargrade=(stu[i].math+stu[i].English+stu[i].C)/3;
        }
    
        if ((fp = fopen("stud", "w")) == NULL) {
                printf("error :cannot open file!\n");
                exit(0);
        }
       
        for (i=0; i<5; i++)
                fprintf(fp,"%d %s %d %d %d %d\n", stu[i].ID,stu[i].name, stu[i].math, stu[i].English,
                stu[i].C,stu[i].avargrade);
    
        fclose(fp);    /* 关闭 */
        return 0;
}
